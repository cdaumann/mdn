from symbol import factor
from pandas import concat
import torch
import numpy as np
import ROOT as root


def uninterleave(xys):
  x1 = xys[:,0:1]
  x2 = xys[:,2:3]
  x3 = xys[:,4:5]
  y1 = xys[:,1:2]
  y2 = xys[:,3:4]
  y3 = xys[:,5:6]
  return \
      np.concatenate([x1, x2, x3], axis=1) \
    , np.concatenate([y1, y2, y3], axis=1)


def doublePxPyPz(ptetaphis):
  p1 , p2 = uninterleave(ptetaphis)
  p1 , p2 = PxPyPz(p1) , PxPyPz(p2)

  return np.concatenate([p1 , p2] , axis=1)


def PxPyPz(ptetaphi):
  pts = ptetaphi[:,0:1]
  etas = ptetaphi[:,1:2]
  phis = ptetaphi[:,2:3]

  pxs = pts * np.cos(phis)
  pys = pts * np.sin(phis)
  pzs = pts * np.sinh(etas)

  isinf = np.isinf(pzs)

  if np.any(isinf):
    print("inf from eta:")
    print(etas[isinf])
    raise ValueError("infinity from sinh(eta)")

  return np.concatenate([pxs, pys, pzs], axis=1)

def get_phi(ptetaphi):


  pxs = ptetaphi[:,0]
  pys = ptetaphi[:,1]
  pzs = ptetaphi[:,2]

  pts = np.sqrt( pxs**2 + pys**2 )

  phis = np.arcsin(pxs/pts)

  return phis


def PxPyPz1(ptetachi):
  return PxPyPz(PtEtaPhi(ptetachi))


def observables(ptetaphis):
  p1 , p2 = uninterleave(ptetaphis)

  pxyz1 = PxPyPz(p1)
  pteta1 = p1[:,:2]

  pxyz2 = PxPyPz(p2)
  pteta2 = p2[:,:2]


  m = invm(pxyz1, pxyz2)
  pt = pttot(pxyz1, pxyz2)

  return np.concatenate ( [ pxyz1, pteta1, pxyz2 , pteta2, m, pt ] , axis=1 )


def PtEtaPhi(pxpypz):
  tmp = ptetachi
  tmp[:,2] = np.arctan(tmp[:,2]) * 2

  return tmp


def doublePtEtaPhi(ptetachis):
  p1 , p2 = uninterleave(ptetaphis)
  p1 , p2 = PtEtaPhi(p1) , PtEtaPhi(p2)

  return np.concatenate([p1 , p2] , axis=1)



def invm_cart(pxpypz1, pxpypz2):
  px1 = pxpypz1[:,0]
  py1 = pxpypz1[:,1]
  pz1 = pxpypz1[:,2]
  e1 = np.sqrt(px1**2 + py1**2 + pz1**2)

  px2 = pxpypz2[:,0]
  py2 = pxpypz2[:,1]
  pz2 = pxpypz2[:,2]
  e2 = np.sqrt(px2**2 + py2**2 + pz2**2)

  m2 = (e1 + e2)**2 - (px1 + px2)**2 - (py1 + py2)**2 - (pz1 + pz2)**2

  # I can see that rounding is a problem in fact!
  m2[m2 < 0] = 0

  return np.sqrt(m2)


def invm(ptetaphi1,ptetaphi2):
  m2 = 2*ptetaphi1[:,0]*ptetaphi2[:,0]*( np.cosh( ptetaphi1[:,1] - ptetaphi2[:,1]) - np.cos( ptetaphi1[:,2] - ptetaphi2[:,2] )  )
  m2[m2 < 0] = 0
  return np.sqrt(m2)


def ditau_eta(p1,p2):
  p = p1 + p2
  px = p[:,0]
  py = p[:,1]
  pz = p[:,2]
  eta = np.arctanh( pz/momentum(p1+p2)) #chenged this last time =p 
  return eta

def pxpy_topt(p1):
  px = p1[:,0]
  py = p1[:,1]
  pt = np.sqrt( px**2 + py**2 ) #chenged this last time =p 
  return pt

def pttot(p1, p2):
  px1 = p1[:,0]
  py1 = p1[:,1]

  px2 = p2[:,0]
  py2 = p2[:,1]

  pt2 = (px1 + px2)**2 + (py1 + py2)**2

  return np.sqrt(pt2)

def px_total(p1, p2):
  px1 = p1[:,0]
  py1 = p1[:,1]

  px2 = p2[:,0]
  py2 = p2[:,1]

  pt2 = (px1 + px2)**2 + (py1 + py2)**2

  pxt = px1+px2
  pyt = py1 + py2

  return pxt

def py_total(p1, p2):
  px1 = p1[:,0]
  py1 = p1[:,1]

  px2 = p2[:,0]
  py2 = p2[:,1]

  pt2 = (px1 + px2)**2 + (py1 + py2)**2

  pxt = px1+px2
  pyt = py1 + py2

  return pyt

def intersperse(i, xs):
  for x in xs[:-1]:
    yield x
    yield i

  yield xs[-1]


def network(nodes, act=torch.nn.LeakyReLU(0.05, inplace=True), init=None, final=None):
  tmp = [ torch.nn.Linear(nodes[i], nodes[i+1], 1) for i in range(len(nodes) - 1) ]

  tmp = list(intersperse(act, tmp))
    
  if init is not None:
    tmp = [init] + tmp
  if final is not None:
    tmp = tmp + [final]

  return torch.nn.Sequential(*tmp)


def regress(net, inputs, m):
  b = inputs.size()[0]
  outs = net(inputs)
  mus = outs[: , :m]
  rest = outs[:, m:]
  cov = uncholesky(uppertriangle(rest, m))
  return (mus, cov)


# good god.
def permutecov(cov, n):
  leftbot = cov[:,0:n,0:n]
  leftmid = cov[:,0:n,n:2*n]
  lefttop = cov[:,0:n,2*n:]

  midbot = cov[:,n:2*n,0:n]
  midmid = cov[:,n:2*n,n:2*n]
  midtop = cov[:,n:2*n,2*n:]

  rightbot = cov[:,2*n:,0:n]
  rightmid = cov[:,2*n:,n:2*n]
  righttop = cov[:,2*n:,2*n:]

  top = torch.cat([midtop, lefttop, righttop], axis=1)
  mid = torch.cat([midbot, leftbot, rightbot], axis=1)
  bot = torch.cat([midmid, leftmid, rightmid], axis=1)

  out = torch.cat([bot, mid, top], axis=2)
  return out



def distloss(targs, mus, cov):
  invcov = torch.linalg.inv(cov)

  deltas = (targs - mus).unsqueeze(dim=1)
  deltasT = torch.transpose(deltas, dim0=1, dim1=2)

  tmp = torch.matmul(invcov, deltasT)

  res = torch.matmul(deltas, tmp).squeeze(2).squeeze(1)

  return res


def loss(targets, mus, cov, n):
  m = mus.size()[1]
  muspermute = torch.cat([mus[:,n:2*n], mus[:,0:n], mus[:,2*n:]], axis=1)
  covpermute = permutecov(cov, n)

  d1 = distloss(targets, mus, cov)
  d2 = distloss(targets, muspermute, covpermute)

  choice = d1 < d2
  choices = choice.unsqueeze(1).expand((-1, m))
  musmin = torch.where(choices, mus, muspermute)
  choices = choice.unsqueeze(1).unsqueeze(1).expand((-1, m, m))
  covmin = torch.where(choices, cov, covpermute)
  dmin = torch.min(d1, d2)

  eigs = torch.nn.functional.relu(torch.real(torch.linalg.eigvals(cov)))
  logdet = torch.sum(torch.log(eigs), axis=1)

  return musmin , covmin , logdet + dmin
  

def covariance(sigmas, correlations):
  nsig = sigmas.size()[1]
  mul1 = torch.cat([sigmas.unsqueeze(dim=2)]*nsig, axis=2)
  mul2 = torch.transpose(mul1, dim0=1, dim1=2)

  return correlations * mul1 * mul2


def fromuppertriangle(xs, n):
  if n == 1:
    return xs.unsqueeze(dim=2)

  else:
    diag = xs[:,:1].unsqueeze(dim=2)
    offdiag = xs[:,1:n].unsqueeze(dim=1)
    rest = fromuppertriangle(xs[:,n:], n-1)

    row = torch.cat([diag, offdiag], axis=2)
    rect = torch.cat([torch.transpose(offdiag, dim0=1, dim1=2), rest], axis=2)
    ret = torch.cat([row, rect], axis=1)
    return ret


def uppertriangle(xs, n):
  if n == 1:
    return xs.unsqueeze(dim=2)

  else:
    diag = xs[:,:1].unsqueeze(dim=2)
    offdiag = xs[:,1:n].unsqueeze(dim=1)
    rest = uppertriangle(xs[:,n:], n-1)

    row = torch.cat([diag, offdiag], axis=2)
    rect = torch.cat([torch.zeros_like(torch.transpose(offdiag, dim0=1, dim1=2)), rest], axis=2)
    ret = torch.cat([row, rect], axis=1)
    return ret


def uncholesky(m):
  return torch.matmul(m, torch.transpose(m, dim0=1, dim1=2))


def inrange(mn , xs , mx):
  return np.logical_and(mn < xs, xs < mx)



def fitGauss(xs):
  avg = np.mean(xs)
  std = np.std(xs)

  return avg, std



def centralGauss(xs):
  avg = np.mean(xs)
  std = np.std(xs)

  masked = xs[inrange(-2*std , xs , 2*std)]

  return fitGauss(masked)


def binnedGauss(bins, xs, ys):
  means = []
  stds = []
  outbincenters = []
  outbinwidths = []
  for i in range(len(bins)-1):
    mask = inrange(bins[i], xs, bins[i+1])
    if not np.any(mask):
      continue

    # mean , std = fitGauss(ys[mask])
    mean , std = centralGauss(ys[mask])
    means.append(mean)
    stds.append(std)
    outbincenters.append((bins[i] + bins[i+1]) / 2)
    outbinwidths.append((bins[i+1] - bins[i]) / 2)
    continue

  return outbincenters, outbinwidths, means , stds


def momentum(pxpypz):
  px = pxpypz[:,0]
  py = pxpypz[:,1]
  pz = pxpypz[:,2]

  return np.sqrt(px **2 + py **2 + pz **2)


# 1 = plus
# 2 = minus
def costhetastar_1(pxpypz1, pxpypz2):
  pz1 = pxpypz1[:,2]
  pz2 = pxpypz2[:,2]

  E1 = momentum(pxpypz1)
  E2 = momentum(pxpypz2)

  sym1 = E1 + pz1
  asym1 = E1 - pz1

  sym2 = E2 + pz2
  asym2 = E2 - pz2

  m = invm_cart(pxpypz1, pxpypz2)

  pt12 = pttot(pxpypz1, pxpypz2)

  return (sym1 * asym2 - sym2 * asym1) / (m *np.sqrt(m * m + pt12 * pt12))


def costhetastar(pxpypz1, pxpypz2):

  pz1 = pxpypz1[:,2]
  pz2 = pxpypz2[:,2]

  E1 = momentum(pxpypz1)
  E2 = momentum(pxpypz2)

  pp1 = (1/np.sqrt(2))*( E1+pz1 )
  pm1 = (1/np.sqrt(2))*( E1-pz1 )

  pp2 = (1/np.sqrt(2))*( E2+pz2 )
  pm2 = (1/np.sqrt(2))*( E2-pz2 )

  m = invm_cart(pxpypz1, pxpypz2)

  pt12 = pttot(pxpypz1, pxpypz2)

  return 2*( pp1*pm2 - pm1*pp2 )/( m*np.sqrt( m*m + pt12*pt12) )

def norm_weights(tname):
      file = root.TFile.Open(tname[:-16], "READ")
      w = file.Get("sumOfWeights")      
      ws = w.GetBinContent(4)

      filtereff = 1.
      xsection  = 0.932

      #xsection for the HH channel
      if( 'mZ_120' in tname ):
        xsection = 0.0231
      if( 'maxHTpTV2' in tname ):
        xsection = 0.932

      if( 'CVetoBVeto' in str(tname) ):
        filtereff = 0.85
      if( 'CFilterBVeto' in str(tname) ):
        filtereff = 0.125
      if( 'BFilter' in str(tname) ):
        filtereff = 0.02479   

      #normalization for the powheg samples
      if( 'PowhegPythia8EvtGen' in str(tname) ):
        if('Ztautau' in str(tname)):
          xsection = 1898E-3
          w = file.Get("rec_weight")
          ws = w.GetBinContent(4) 
        if( '120M180' in str(tname)):
          xsection = 17.476e-3
        if( '180M250' in str(tname)):  
          xsection = 2.9213e-3 
        if( '250M400' in str(tname)):  
          xsection = 1.0819e-3 
        if( '400M600' in str(tname)):  
          xsection = 0.19551e-3
        if( '600M800' in str(tname)):  
          xsection = 0.037402e-3
        if( '800M1000' in str(tname)):  
          xsection = 0.010607e-3
        if( '1000M1250' in str(tname)):  
          xsection = 0.0042585e-3
        if( '1250M1500' in str(tname)):  
          xsection = 0.001422e-3
        if( '1500M1750' in str(tname)):  
          xsection = 0.00054525e-3
        if( '1750M2000' in str(tname)):  
          xsection = 0.00022993e-3
        if( '2000M2250' in str(tname)):  
          xsection = 0.00010386e-3
        if( '2250M2500' in str(tname)):  
          xsection = 4.9403e-08
        if( '2500M2750' in str(tname)):  
          xsection = 2.4454e-08
        if( '2750M3000' in str(tname)):  
          xsection = 1.2489e-08          
        filtereff = 1


      if('Ztautau_HH' in str(tname)):
        xsection = 0.932

        if( 'CVetoBVeto' in str(tname) ):
          filtereff = 0.85
        if( 'CFilterBVeto' in str(tname) ):
          filtereff = 0.125
        if( 'BFilter' in str(tname) ):
          filtereff = 0.02479   

      if('Wtaunu_H' in str(tname)):
        xsection = 14.126
        if( 'CVetoBVeto' in str(tname) ):
          filtereff = 0.85
        if( 'CFilterBVeto' in str(tname) ):
          filtereff = 0.14337
        if( 'BFilter' in str(tname) ):
          filtereff = 0.0087  

      if('Wtaunu_L' in str(tname)):
        xsection = 7.68
        if( 'CVetoBVeto' in str(tname) ):
          filtereff = 0.85
        if( 'CFilterBVeto' in str(tname) ):
          filtereff = 0.14337
        if( 'BFilter' in str(tname) ):
          filtereff = 0.0087


      if('ttbar' in str(tname) and 'allhad' in str(tname) ):
        xsection = 0.72977
        filtereff = 0.5438225
      if('ttbar' in str(tname) and 'nonallhad' in str(tname) ):
        xsection = 0.72977
        filtereff = 0.5438225

      #now the luminosity
      if('r10724' in str(tname)):
          F = (58.5e+6)*(xsection*filtereff)/ws  
      if('r10201' in str(tname)):
          F = (44.3e+6)*(xsection*filtereff)/ws
      if('r9364' in str(tname)):
          F = (36.1e+6)*(xsection*filtereff)/ws  


      return F    

#this function aplies a mask in a [nxm] array, like the feats which has [:,n] dimesionsion, n being the number of inputs
def apply_feat_mask(feats,mask):
  ster = [] #array to save and keep lines of the feats matrix

  for i in range( len(feats[:,0]) ):
    jec = np.array(feats[:,i]).reshape( len(feats[:,i]) ,1)
    jec = np.array(torch.tensor( jec )[mask])
    jec = jec.reshape( len(jec) ,1)

    if(i == 0 ):
      ster = jec
    else:
      ster = np.concatenate([ster, jec ], axis=1)    
  masked_feats = torch.tensor(ster)
  return masked_feats


# this creates histograms with given binning of sources and targets,
# fits the ratio of these histograms to a polynomial of given degree,
# then throws nthrows samples from sources,
# reweighted based on the fitted polynomial ratio.

import numpy

def downsample_to(sources, binning, degree, nthrows):
  #targetys , _ = numpy.histogram(targets, bins=binning)
  sourceys , _ = numpy.histogram(sources, bins=binning)
  bs = (binning[:-1] + binning[1:]) / 2.0

  ratio = 1 / sourceys

  coeffs = numpy.polynomial.polynomial.polyfit(bs, ratio, degree)

  sourcewgts = numpy.polynomial.polynomial.polyval(sources, coeffs)

  # deal with negative numbers
  sourcewgts[sourcewgts < 1e-9] = 1e-9

  sourcewgts = sourcewgts / numpy.sum(sourcewgts)

  rng = numpy.random.default_rng()

  #the question is, i need to have another mask for the choosen events?

  #return rng.choice ( numpy.arange(sources.size), size=nthrows, p=sourcewgts.flatten() )

  return  rng.choice ( numpy.arange(sources.shape[0]), size=nthrows, p=sourcewgts.flatten())

def line(x):
  return -3.052*x+6300

def my_downsample(target):
  bin = [0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1400,3000]

  first = np.asarray(np.where( target < 100 ))
  second = np.asarray(np.asarray(np.where( np.logical_and( target > 100 , target < 250  ) )))
  third = np.asarray(np.where( np.logical_and( target > 250 , target < 500  ) ))
  fourth = np.asarray(np.where( np.logical_and( target > 500 , target < 800  ) ))
  sixth = np.asarray(np.where( np.logical_and( target > 800 , target < 1500  ) ))
  seventh = np.asarray(np.where( np.logical_and( target > 1500 , target < 7000  ) ))

  print( np.shape(seventh), np.shape(sixth)[1], np.shape(second) )

  tamanho = np.shape(sixth)[1]
  first = first[0,:int(tamanho/5)]
  second = second[0,:int(tamanho)]
  third = third[0,:int(tamanho)]
  sixth = sixth[0,:tamanho]
  fourth = fourth[0,:int(tamanho/0.9)]
  seventh = seventh[0,:tamanho]

  print( 'again: ', np.shape(seventh),  np.shape(second) )

  hue = np.concatenate(  [first,second,third,fourth,sixth,seventh], axis = 0 )

  

  for i in range(40):
    keep = np.asarray(np.where( np.logical_and( target > i*50 , target < (i+1)*50 )))
    factor = int(line((i+0.5)*50))
    keep = keep[0,:factor]

    if(i==0):
      hue = keep
    else:
      hue = np.concatenate([hue,keep],axis=0)  

  return hue

def downsample_onceagain(target):
  bin = [0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1400,3000]

  print('total training: ', len(target))

  events = []
  for i in range(90):
    #start = i*25
    #target = (i+1)*25
    events.append( np.asarray(np.asarray(np.where( np.logical_and( target > (i)*25 , target < (i+1)*25  ) )))   )

  sixth = np.asarray(np.where( np.logical_and( target > 1975 , target < 2000  ) ))
  tamanho = np.shape(sixth)[1]

  print('tamanho: ', tamanho)

  for i in range(len(events)):
    #events[i] = np.array(events[i][0,:int(line(i*25)) ])
    events[i] = np.array(events[i][0,:int(tamanho)])

  #print( 'again: ', np.shape(seventh),  np.shape(second) )

  #concatenating everything
  print(len(events))
  print(len(events[8]))
  print(np.shape(events[8]))

  for i in range(len(events)):
    if(i==0):
      hue = events[i]
    else:
      hue = np.concatenate(  [hue,events[i]], axis = 0 )


  print( np.shape( hue ) )

  #hue = np.concatenate(  [first,second,third,fourth,sixth,seventh], axis = 0 )



  return hue  