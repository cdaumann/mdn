# This code is aimed to make the validation and performance plots for the MDN + Deep sets network
#
#
#

from ctypes import util
from hashlib import sha1
from locale import T_FMT_AMPM
from unicodedata import name
import ROOT as root
import torch
from sys import argv
from IO import *
import json
import matplotlib.figure as figure
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from plot_funcs import calculate_ptditau_bias_and_reso,calculate_z_eta,plot_eff, calculate_leading_tau_bias_and_reso,plot_1d_distributions,calculate_mass_bias_and_reso,calculate_costhetastar
import root_numpy as rnp


#recreating the nentwork and everything
fconfig = open(argv[1])
config = json.load(fconfig)
fconfig.close()

device = config["device"]


# ====  end of loading ====

valid_tnames = config["valid_tnames1"]
dset = Dataset(valid_tnames, valid_frac=0.99, max=8500000)
targlen = dset.targs.size()[1]

regressnodes =  [ 10 ] + config["regressnodes"] + [ targlen + (targlen * (targlen+1) // 2) ]
act = torch.nn.LeakyReLU(0.01, inplace=True)
regression = utils.network(regressnodes, act=act, init=dset.normfeats)
networks = [regression]

#signalizing to train the network in the 'cpu'
for net in networks:
  net.to(device)

#loading the weights and everything xD
path = "models"

regression.load_state_dict(torch.load("regression10.pth"))  

print(targlen + (targlen * (targlen+1) // 2))

targets = dset.validtargs
mus , cov = utils.regress ( regression, dset.validfeats, targlen)
sigmas = np.sqrt(np.diagonal(cov.detach().numpy(), axis1=1, axis2=2))


#calculating the relative bias and resolution of mass estimation methods
sigs = sigmas[:,len(mus[0,:])-1]
tgs = targets[: , len(mus[0,:])-1]
thesemus = mus[:,len(mus[0,:])-1]
calculate_mass_bias_and_reso(tgs,dset.ttm_mass,thesemus,dset.smin_mass,dset.mmc_mass,dset.pesos,sigs,sigmas,mus)


