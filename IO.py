from cmath import pi
from operator import truth
from re import I
import uproot
import awkward as ak
import numpy as np
import torch
import utils
import ROOT as root
import root_numpy as rnp



taumomnames = \
  [ "TauPt"
  , "TauEta"
  , "TauPhi"
  ]

taufeatnames = \
  [ "TauNCoreTracks"
  ]


elfeatnames = \
  [ "ElePt"
  , "EleEta"
  , "ElePhi"
  ]

mufeatnames = \
  [ "MuonPt"
  , "MuonEta"
  , "MuonPhi"
  ]

metfeatnames = \
  [ "MET_etx"
  , "MET_ety"
  ]



tautargnames = [ "TruthTauPt" , "TruthTauEta", "TruthTauPhi" ]
neutritargnames = [ "TruthNeutrinoPt" , "TruthNeutrinoEta", "TruthNeutrinoPhi" ]
# eventtargnames = [ "TruthMET_etx" , "TruthMET_ety" ]


def normalize(xs):
  mus = torch.mean(xs, axis=0)
  sigs = torch.std(xs, axis=0)

  # if the std is too small don't blow up.
  sigs[sigs < 1e-4] = 1e-4

  l = len(mus)

  transf = torch.nn.Linear(l, l)

  transf.weight.data = torch.diag(1.0 / sigs)
  transf.bias.data = -mus / sigs
  transf.weight.requires_grad = False
  transf.bias.requires_grad = False

  return transf


def invnormalize(xs):
  mus = torch.mean(xs, axis=0)
  sigs = torch.std(xs, axis=0)
  l = len(mus) * 2

  transfinvpdf = torch.nn.Linear(l, l)

  transfinvpdf.weight.data = torch.diag(torch.cat([sigs, sigs]))
  transfinvpdf.bias.data = torch.cat([mus, torch.zeros_like(mus)])
  transfinvpdf.weight.requires_grad = False
  transfinvpdf.bias.requires_grad = False

  return transfinvpdf


def maskedBranch(t, b, mask, mask_two):
  #print('old: ', t[b].array()[mask] )
  #t[b].array()[mask]
  return (t[b].array()[mask])[mask_two]
  #return t.arrays([b], mask)

def maskedBranch_1(t, b, mask):
  #print( 'new: ', t.arrays([b], mask) )
  return 0

# TODO
# this won't work for very large datasets.
# also not set up for GPUs yet

class Dataset:
  def __init__(self, tnames, valid_frac=0.3, max=-1):

    taufeats = None
    mufeats = None
    elfeats = None
    metfeats = None
    pesitos = None
    mmc_mass = None
    ttm_mass = None
    smin_mass = None
    smin_ditaupt = None
    t_mass = None
    r_mass = None
    r_pt = None

    r_lead_pt = None
    smin_lead_pt = None

    decay_type = None

    mask_HH = None #i create this one just to separete the validation into HH and LH

    third_mask = None

    
    targs = None

    for tname in tnames:
      t = uproot.reading.open(tname)

      print('file: ', str(tname))

      #separating the files into hh,eh and mh. This is nescessary because to buld the reco mass and pt arrays.      
      if('T_s2thh_NOMINAL' in str(tname)):


        passTrue = ak.sum(np.abs(t["TruthTauEta"].array()) < 2.5, axis=1) == 2
        #passTrue = ak.sum(np.abs(t["passReco"].array()) == True) == 2
        passTrue = np.logical_and( passTrue, t["passTruth"].array()  )
        passTrue = np.logical_and( passTrue, t["stitch_mll_pass"].array()  ) #bulk samples must have born mtautau < 120 GeV

        nrecotaus = ak.num(np.abs(t["TauEta"].array()) < 2.5, axis=1)
        nrecomus = ak.num(np.abs(t["MuonEta"].array()) < 2.5, axis=1)
        nrecoels = ak.num(np.abs(t["EleEta"].array()) < 2.5, axis=1)
        passReco = np.logical_and(np.logical_and(nrecotaus == 2, nrecomus == 0), nrecoels == 0)
        passReco = np.logical_and( passReco, t["passReco"].array()  )

        #requiring truth mass higher than 100 GeV

        mask = np.logical_and(passReco,passTrue)

        reco_array = t.arrays([ "TauPt", "TauEta", "TauPhi","TauE"])
        reco_array = reco_array[mask]
        reco_mass = np.sqrt(2*reco_array.TauPt[:,0]*reco_array.TauPt[:,1]*(np.cosh( reco_array.TauEta[:,0]  - reco_array.TauEta[:,1]  ) -np.cos( reco_array.TauPhi[:,0] - reco_array.TauPhi[:,1]  ))   )

        #reco ditau pt
        reco_array = t.arrays([ "TauPt", "TauEta", "TauPhi","TauE"])
        reco_array = reco_array[mask]
        reco_pt = np.sqrt( ( reco_array.TauPt[:,0]*np.cos(reco_array.TauEta[:,0]) + reco_array.TauPt[:,1]*np.cos(reco_array.TauEta[:,1])    )**2 + ( reco_array.TauPt[:,0]*np.sin(reco_array.TauEta[:,0]) + reco_array.TauPt[:,1]*np.sin(reco_array.TauEta[:,1])    )**2   )
        reco_pt = ak.to_numpy(reco_pt).reshape(-1, 1) 

        decay = np.ones_like(reco_pt)
        decay = decay[:]*0
        decay = decay.reshape(-1, 1)
        print(decay)

        hh_mask = np.ones_like(reco_mass)

      if('T_s2teh_NOMINAL' in str(tname)):


        passTrue = ak.sum(np.abs(t["TruthTauEta"].array()) < 2.5, axis=1) == 2
        #passTrue = ak.sum(np.abs(t["passReco"].array()) == True) == 2
        passTrue = np.logical_and( passTrue, t["passTruth"].array()  )
        passTrue = np.logical_and( passTrue, t["stitch_mll_pass"].array()  ) #bulk samples must have born mtautau < 120 GeV

        nrecotaus = ak.num(np.abs(t["TauEta"].array()) < 2.5, axis=1)
        nrecomus = ak.num(np.abs(t["MuonEta"].array()) < 2.5, axis=1)
        nrecoels = ak.num(np.abs(t["EleEta"].array()) < 2.5, axis=1)
        passReco = np.logical_and(np.logical_and(nrecotaus == 1, nrecomus == 0), nrecoels == 1)
        passReco = np.logical_and( passReco, t["passReco"].array()  )

        #requiring truth mass higher than 100 GeV

        mask = np.logical_and(passReco,passTrue)

        tau_array = t.arrays([ "TauPt", "TauEta", "TauPhi","TauE"])
        ele_array = t.arrays([ "ElePt", "EleEta", "ElePhi","EleE"])
        tau_array = tau_array[mask]
        ele_array = ele_array[mask]
        reco_mass = np.sqrt(2*tau_array.TauPt[:,0]*ele_array.ElePt[:,0]*(np.cosh( tau_array.TauEta[:,0]  - ele_array.EleEta[:,0]  ) -np.cos( tau_array.TauPhi[:,0] - ele_array.ElePhi[:,0]  ))   )

        #reco ditau pt
        reco_pt = np.sqrt( ( tau_array.TauPt[:,0]*np.cos(tau_array.TauEta[:,0]) + ele_array.ElePt[:,0]*np.cos(ele_array.EleEta[:,0])    )**2 + ( tau_array.TauPt[:,0]*np.sin(tau_array.TauEta[:,0]) + ele_array.ElePt[:,0]*np.sin(ele_array.EleEta[:,0])    )**2   )
        reco_pt = ak.to_numpy(reco_pt).reshape(-1, 1) 

        decay = np.ones_like(reco_pt)
        decay = decay[:]*1
        decay = decay.reshape(-1, 1)

        hh_mask = np.ones_like(reco_mass)*0

      if('T_s2tmh_NOMINAL' in str(tname)):


        passTrue = ak.sum(np.abs(t["TruthTauEta"].array()) < 2.5, axis=1) == 2
        #passTrue = ak.sum(np.abs(t["passReco"].array()) == True) == 2
        passTrue = np.logical_and( passTrue, t["passTruth"].array()  )
        passTrue = np.logical_and( passTrue, t["stitch_mll_pass"].array()  ) #bulk samples must have born mtautau < 120 GeV

        nrecotaus = ak.num(np.abs(t["TauEta"].array()) < 2.5, axis=1)
        nrecomus = ak.num(np.abs(t["MuonEta"].array()) < 2.5, axis=1)
        nrecoels = ak.num(np.abs(t["EleEta"].array()) < 2.5, axis=1)
        passReco = np.logical_and(np.logical_and(nrecotaus == 1, nrecomus == 1), nrecoels == 0)
        passReco = np.logical_and( passReco, t["passReco"].array()  )

        #requiring truth mass higher than 100 GeV
        mask = np.logical_and(passReco,passTrue)

        tau_array = t.arrays([ "TauPt", "TauEta", "TauPhi","TauE"])
        muon_array = t.arrays([ "MuonPt", "MuonEta", "MuonPhi","MuonE"])
        tau_array = tau_array[mask]
        muon_array = muon_array[mask]
        reco_mass = np.sqrt(2*tau_array.TauPt[:,0]*muon_array.MuonPt[:,0]*(np.cosh( tau_array.TauEta[:,0]  - muon_array.MuonEta[:,0]  ) -np.cos( tau_array.TauPhi[:,0] - muon_array.MuonPhi[:,0]  ))   )

        #reco ditau pt
        reco_pt = np.sqrt( ( tau_array.TauPt[:,0]*np.cos(tau_array.TauEta[:,0]) + muon_array.MuonPt[:,0]*np.cos(muon_array.MuonEta[:,0])    )**2 + ( tau_array.TauPt[:,0]*np.sin(tau_array.TauEta[:,0]) + muon_array.MuonPt[:,0]*np.sin(muon_array.MuonEta[:,0])    )**2   )
        reco_pt = ak.to_numpy(reco_pt).reshape(-1, 1) 

        decay = np.ones_like(reco_pt)
        decay = decay[:]*2
        decay = decay.reshape(-1, 1)

        hh_mask = np.ones_like(reco_mass)*0
      

      #calculating the truth mass by other means 
      arrays = t.arrays([ "TruthTauPt", "TruthTauEta", "TruthTauPhi","TruthTauM"])
      arrays = arrays[mask]
      truthmass = np.sqrt(2*arrays.TruthTauPt[:,0]*arrays.TruthTauPt[:,1]*(np.cosh( arrays.TruthTauEta[:,0]  -arrays.TruthTauEta[:,1]  ) -np.cos( arrays.TruthTauPhi[:,0] - arrays.TruthTauPhi[:,1]  ))   )

      
      if('maxHTpTV2' in str(tname)):
        truth = np.ones_like(truthmass)*75
        mask_two = np.greater(reco_mass/1000., truth)
      else:
        truth = np.ones_like(truthmass)*0
        mask_two = np.greater(reco_mass/1000., truth)

      truthmass = truthmass[ mask_two]
      truthmass = ak.to_numpy(truthmass).reshape(-1, 1)
            
      #requiring only events with truth mass > 120 for validation studies      
      if('maxHTpTV2' in str(tname)):
        truth = np.ones_like(truthmass)*120
        mask_three = np.greater(0, truth)
      else:
        truth = np.ones_like(truthmass)*0
        mask_three = np.greater(truthmass/1000., truth)        

      if third_mask is None:
        third_mask = mask_three.flatten()
      else:
        third_mask = np.concatenate([third_mask, mask_three.flatten()], axis = 0)

      #end of masks -------------------------------------------------------------------------------------------


      #here i am declarying and creating a kinda of mask, the objective is to separete the
      #training dataset into HH and LH. But the training dataset still hass both LH and HH.
      if decay_type is None:
        decay_type = decay[mask_two]
      else:
        decay_type = np.concatenate( [decay_type, decay[mask_two]], axis = 0 )  

      if mask_HH is None:
        mask_HH = hh_mask[mask_two]
      else:
        mask_HH = np.concatenate( [mask_HH, hh_mask[mask_two]], axis = 0 )  


      # ===================== normalizing weights =================================
      pesos =  ak.to_numpy(maskedBranch(t, 'mcWeight', mask, mask_two)).reshape(-1, 1)
      norm_factor = utils.norm_weights(tname)
      pesos = pesos*norm_factor
      # =================== end of weights normalization ============================

      if pesitos is None:
        pesitos = pesos
      else:
        pesitos = np.concatenate( [pesitos, pesos], axis = 0 )  

      # =========== Keeping information about the other methods in the market =======
      total_mT_m =  ak.to_numpy(maskedBranch(t, 'ttm_mass', mask, mask_two)).reshape(-1, 1)
      smin_m =  ak.to_numpy(maskedBranch(t, 'smim_mass', mask, mask_two)).reshape(-1, 1)
      smin_zpt =  ak.to_numpy(maskedBranch(t, 'smim_ditaupt', mask, mask_two)).reshape(-1, 1)
      mmc_m = ak.to_numpy(maskedBranch(t, 'mmc_mass_mlm', mask, mask_two)).reshape(-1, 1)

      if mmc_mass is None: #mmc method
        mmc_mass = mmc_m
      else:
        mmc_mass = np.concatenate( [mmc_mass, mmc_m], axis = 0 )  

      if ttm_mass is None: #total transverse mass
        ttm_mass = total_mT_m
      else:
        ttm_mass = np.concatenate( [ttm_mass, total_mT_m], axis = 0 )  


      if smin_mass is None: #smin transverse mass
        smin_mass = smin_m
      else:
        smin_mass = np.concatenate( [smin_mass, smin_m ], axis = 0 )  

      if smin_ditaupt is None: #smin transverse mass
        smin_ditaupt = smin_zpt
      else:
        smin_ditaupt = np.concatenate( [smin_ditaupt, smin_zpt ], axis = 0 )  

      #leading tau pt

      reco_lpt =  ak.to_numpy(maskedBranch(t, 'TauPt', mask, mask_two)).reshape(-1, 1)

      if r_lead_pt is None: #smin transverse mass
        r_lead_pt = reco_lpt
      else:
        r_lead_pt = np.concatenate( [r_lead_pt, reco_lpt ], axis = 0 )       

      smin_lpt =  ak.to_numpy(maskedBranch(t, 'smim_tau1pt', mask, mask_two)).reshape(-1, 1)

      if smin_lead_pt is None: #smin transverse mass
        smin_lead_pt = smin_lpt
      else:
        smin_lead_pt = np.concatenate( [smin_lead_pt, smin_lpt ], axis = 0 )  


      #truth mass now
      true_array = t.arrays([ "TruthTauPt", "TruthTauEta", "TruthTauPhi","TruthTauM"])
      true_array = true_array[mask]
      truth_mass = np.sqrt(2*true_array.TruthTauPt[:,0]*true_array.TruthTauPt[:,1]*(np.cosh( true_array.TruthTauEta[:,0]  -true_array.TruthTauEta[:,1]  ) -np.cos( true_array.TruthTauPhi[:,0] - true_array.TruthTauPhi[:,1]  ))   )
      truth_mass = ak.to_numpy(truth_mass).reshape(-1, 1)      

      if t_mass is None:
        t_mass = truth_mass[mask_two]/1000.
      else:
        t_mass =   np.concatenate( [t_mass, truth_mass[mask_two]/1000. ], axis = 0 )  


      #contitnuing on reco mass    
      reco_mass = ak.to_numpy(reco_mass).reshape(-1, 1)
       
      if r_mass is None:
        r_mass = reco_mass[mask_two]/1000.
      else:
        r_mass =   np.concatenate( [r_mass, reco_mass[mask_two]/1000. ], axis = 0 )       


      if r_pt is None:
        r_pt = reco_mass[mask_two]/1000.
      else:
        r_pt =   np.concatenate( [r_pt, reco_pt[mask_two]/1000. ], axis = 0 )     


      # ============= end of other methods gathering xD =================

      # clean up boilerplate?!
      # event features are expected to be 1D
      tmp = [ ak.to_numpy(maskedBranch(t, k, mask, mask_two)).reshape(-1, 1).astype(np.float32)  for k in metfeatnames ]

      tmp = np.concatenate(tmp, axis=1) / 1e3
      if metfeats is None:
        metfeats = tmp
      else:
        metfeats = np.concatenate([ metfeats , tmp ] , axis=0 )

      tmp = [ ak.to_numpy(ak.fill_none(ak.pad_none(maskedBranch(t, k, mask, mask_two), 2), 0, axis=1)) for k in taumomnames ]

      tmp = utils.doublePxPyPz(np.concatenate(tmp, axis=1)) / 1e3
      tmp1 = [ ak.to_numpy(ak.fill_none(ak.pad_none(maskedBranch(t, k, mask, mask_two), 2), 0, axis=1)) for k in taufeatnames ]
      tmp = np.concatenate([ tmp ] +  tmp1, axis=1)
      if taufeats is None:
        taufeats = tmp
      else:
        taufeats = np.concatenate([ taufeats , tmp ] , axis=0 )

      #mufeats and elefeats are not very usefull in the HH channel ...

      tmp = [ ak.to_numpy(ak.fill_none(ak.pad_none(maskedBranch(t, k, mask, mask_two), 2), 0, axis=1)) for k in elfeatnames ]

      tmp = utils.doublePxPyPz(np.concatenate(tmp, axis=1)) / 1e3
      if elfeats is None:
        elfeats = tmp
      else:
        elfeats = np.concatenate([ elfeats , tmp ] , axis=0 )


      tmp = [ ak.to_numpy(ak.fill_none(ak.pad_none(maskedBranch(t, k, mask, mask_two), 2), 0, axis=1)) for k in mufeatnames ]

      tmp = utils.doublePxPyPz(np.concatenate(tmp, axis=1)) / 1e3
      if mufeats is None:
        mufeats = tmp
      else:
        mufeats = np.concatenate([ mufeats , tmp ] , axis=0 )



      #############################################

      tmp = [ ak.to_numpy(ak.fill_none(ak.pad_none(maskedBranch(t, k, mask, mask_two) , 2) , 0, axis=1)) for k in tautargnames]


      #reading the other variables
      ptetaphi1 , ptetaphi2 = utils.uninterleave(np.concatenate(tmp, axis=1))
      ptetaphi1[:,0] /= 1e3
      ptetaphi2[:,0] /= 1e3
      pxyz1 , pxyz2 = utils.PxPyPz(ptetaphi1) , utils.PxPyPz(ptetaphi2)
      pttot = utils.pttot(pxyz1, pxyz2).reshape((-1, 1))
      z_eta = utils.ditau_eta(pxyz1, pxyz2).reshape((-1, 1))

      invm = truthmass/1000.

      tmp = np.concatenate([pxyz1, pxyz2,ptetaphi1[:,:1], ptetaphi2[:,:1], pttot, invm], axis=1)


      #number of targets per taus - this is used to make the loss function tau permutation invariant xD
      self.permutedtargs = 3

      if targs is None:
        targs = tmp
      else:
        targs = np.concatenate([ targs , tmp ] , axis=0 )
        
    self.feats = torch.Tensor( np.concatenate( [ taufeats, metfeats ], axis=1) )
    
    self.targs = torch.Tensor(targs)
    self.pesos = torch.Tensor(pesitos)
    self.ttm_mass = torch.Tensor(ttm_mass)
    self.smin_mass = torch.Tensor(smin_mass)
    
    self.mmc_mass = torch.Tensor(mmc_mass)
    self.t_mass = torch.Tensor(t_mass)
    self.r_mass = torch.Tensor(r_mass)

    mask_LH = torch.tensor(1-mask_HH,dtype=torch.bool)
    mask_HH = torch.tensor(mask_HH,dtype=torch.bool)

    third_mask = torch.tensor(third_mask,dtype=torch.bool)

    #ditau system pT
    self.r_pt = torch.Tensor(r_pt)
    self.smin_ditaupt = torch.Tensor(smin_ditaupt)

    #leading tau pt variables
    self.smin_lead_pt = torch.Tensor(smin_lead_pt)
    self.r_lead_pt = torch.Tensor( r_lead_pt)

    self.normfeats = normalize(self.feats)
    self.invnormtargs = invnormalize(self.targs)

    self.size = self.feats.size()[0]

    #shuffling the data
    perm = torch.randperm(self.size)

    self.feats = self.feats[perm]
    self.targs = self.targs[perm]
    self.pesos = self.pesos[perm]
    self.mmc_mass = self.mmc_mass[perm]
    self.ttm_mass = self.ttm_mass[perm]
    self.smin_mass = self.smin_mass[perm]
    self.smin_ditaupt = self.smin_ditaupt[perm]
    self.t_mass = self.t_mass[perm]
    self.r_mass = self.r_mass[perm]
    self.r_pt = self.r_pt[perm]
    self.smin_lead_pt = self.smin_lead_pt[perm]
    r_lead_pt = r_lead_pt[perm]
    mask_HH = mask_HH[perm]
    mask_LH = mask_LH[perm]
    third_mask = third_mask[perm]


    #separating the dataset into training and validation samples
    valididx = int(self.size * valid_frac)

    #validation datasets now
    self.validfeats = self.feats[:valididx]
    self.validtargs = self.targs[:valididx]
    self.pesos = self.pesos[:valididx]
    self.ttm_mass = self.ttm_mass[:valididx]
    self.mmc_mass = self.mmc_mass[:valididx]
    self.smin_mass = self.smin_mass[:valididx]
    self.smin_ditaupt = self.smin_ditaupt[:valididx]
    self.t_mass = self.t_mass[:valididx]
    self.r_mass = self.r_mass[:valididx]
    self.r_pt = self.r_pt[:valididx]
    self.r_lead_pt = self.r_lead_pt[:valididx]
    self.smin_lead_pt = self.smin_lead_pt[:valididx]
    mask_HH = mask_HH[:valididx]
    mask_LH = mask_LH[:valididx]
    third_mask = third_mask[:valididx]

    #applying third mas on the validation dataset aka only sliced samples in the validation dataset

    self.validfeats =  self.validfeats[third_mask] #self.validfeats[mask_HH]
    self.validtargs = self.validtargs[third_mask]
    self.pesos = self.pesos[third_mask]
    self.ttm_mass = self.ttm_mass[third_mask]
    self.mmc_mass = self.mmc_mass[third_mask]
    self.smin_mass = self.smin_mass[third_mask]
    self.smin_ditaupt = self.smin_ditaupt[third_mask]
    self.t_mass = self.t_mass[third_mask]
    self.r_mass = self.r_mass[third_mask] #self.r_mass[mask_HH]
    self.r_pt = self.r_pt[third_mask]
    self.r_lead_pt = self.r_lead_pt[third_mask]
    self.smin_lead_pt = self.smin_lead_pt[third_mask]
    mask_LH = mask_LH[third_mask]
    mask_HH = mask_HH[third_mask]


    # =========== training sample setup =====================================================================
    self.feats = self.feats[valididx:]
    self.targs = self.targs[valididx:]
    
    self.size = self.feats.size()[0]

    return


  def batch(self, n):
    idxs = torch.randint(low=0, high=self.size, size=(n,))
    return ( self.feats[idxs] , self.targs[idxs] )

