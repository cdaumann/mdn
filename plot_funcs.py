from cmath import pi
from typing import final
import ROOT as root
import torch
from sys import argv
from IO import *
import json
import matplotlib.figure as figure
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import atlasplots as aplt
import root_numpy as rnp


def calculate_mass_bias_and_reso(targs,ttm_mass,thesemus,smin_mass,mmc_mass,pesos,unc,sigmas,mus,channel='HH'):

    bin_size = 100
    initial_bin = 100
    final_bin = 2000

    n_iterations = int((final_bin - initial_bin)/bin_size)

    ttm_bias = root.TH1F( 'ttm_bias', 'ttm_bias', 100, -5,5  )
    nn_bias = root.TH1F( 'nn_bias', 'nn_bias', 100, -5,5  )
    cut_nn_bias = root.TH1F( 'cut_nn_bias', 'cut_nn_bias', 100, -5,5  )
    smin_bias = root.TH1F( 'smin_bias', 'smin_bias', 100, -5,5  )
    mmc_bias = root.TH1F( 'mmc_bias', 'mmc_bias', 100, -5,5  )

    gr_ttm = root.TGraphErrors()
    gr_nn = root.TGraphErrors()
    gr_smin = root.TGraphErrors()
    gr_mmc = root.TGraphErrors()
    gr_ttm_help = root.TGraphErrors()

    reso_gr_ttm = root.TGraphErrors()
    reso_gr_nn = root.TGraphErrors()
    reso_gr_smin = root.TGraphErrors()
    reso_gr_mmc = root.TGraphErrors()

    ttm_mass = ttm_mass.detach().numpy().reshape(len(ttm_mass))
    smin_mass = smin_mass.detach().numpy().reshape(len(smin_mass))
    mmc_mass = mmc_mass.detach().numpy().reshape(len(mmc_mass))
    thesemus = np.array(thesemus.detach().numpy()).reshape(len(thesemus))
    targs = np.array(targs.detach().numpy()).reshape(len(targs))
    pesos = np.array(pesos).reshape( len(pesos) )     


    cut_gr_nn = root.TGraphErrors()
    cut_reso_gr_nn = root.TGraphErrors()

    #pt1 mus and sigmas
    mass_index = len(mus[0,:])-1    
    



    keep_mmc,keep_reco_lpt,keep_nn_lpt,keep_true_lpt,keep_smin_lpt,keep_pesos = mmc_mass,ttm_mass, thesemus, targs, smin_mass,pesos

    for i in range(n_iterations):

        #reseting the arrays
        mmc_lpt,reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = keep_mmc,keep_reco_lpt,keep_nn_lpt,keep_true_lpt,keep_smin_lpt,keep_pesos

        #applying the cut
        mmc_lpt,reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = mmc_lpt[ true_lpt > initial_bin + (i)*bin_size ],reco_lpt[ true_lpt > initial_bin + (i)*bin_size ],nn_lpt[ true_lpt > initial_bin + (i)*bin_size ],true_lpt[ true_lpt > initial_bin + (i)*bin_size ],smin_lpt[ true_lpt > initial_bin + (i)*bin_size ],pesos[ true_lpt > initial_bin + (i)*bin_size ]
        mmc_lpt,reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = mmc_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],reco_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],nn_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],true_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],smin_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],pesos[ true_lpt < initial_bin + (i+1)*bin_size ]

        #rel_diff is a numpy array, i will fill a histogram with it and use root tools to extract mean and std and errors.
        nn_rel_diff = (nn_lpt - true_lpt)/(true_lpt)
        reco_rel_diff = (reco_lpt - true_lpt)/(true_lpt)
        smin_rel_diff = (smin_lpt-true_lpt)/(true_lpt)
        mmc_rel_diff = (mmc_lpt - true_lpt)/(true_lpt)

        rnp.fill_hist(nn_bias,nn_rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(mmc_bias,mmc_rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(ttm_bias,reco_rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(smin_bias,smin_rel_diff, weights=np.array(pesos).reshape(len(pesos)))

        #filling the tgraph errors

        gr_ttm.SetPoint(gr_ttm.GetN(), initial_bin + (i+0.5)*bin_size , ttm_bias.GetMean()  )
        gr_ttm.SetPointError(gr_ttm.GetN(),  bin_size/2. , ttm_bias.GetMeanError()  )

        gr_ttm_help.SetPoint(gr_ttm.GetN(), initial_bin + (i+0.5)*bin_size , ttm_bias.GetMean()  )
        gr_ttm_help.SetPointError(gr_ttm.GetN(),  bin_size/2. , ttm_bias.GetMeanError()  )

        gr_nn.SetPoint(i, initial_bin + (i+0.5)*bin_size , nn_bias.GetMean()  )
        gr_nn.SetPointError(i,  bin_size/2. , nn_bias.GetMeanError()  )

        cut_gr_nn.SetPoint(i, initial_bin + (i+0.5)*bin_size , cut_nn_bias.GetMean()  )
        cut_gr_nn.SetPointError(i,  bin_size/2. , cut_nn_bias.GetMeanError()  )



        gr_smin.SetPoint(i, initial_bin + (i+0.5)*bin_size , smin_bias.GetMean()  )
        gr_smin.SetPointError(i,  bin_size/2. , smin_bias.GetMeanError()  )

        gr_mmc.SetPoint(i, initial_bin + (i+0.5)*bin_size , mmc_bias.GetMean()  )
        gr_mmc.SetPointError(i,  bin_size/2. , mmc_bias.GetMeanError()  )

        reso_gr_ttm.SetPoint(i, initial_bin + (i+0.5)*bin_size , ttm_bias.GetRMS()  )
        reso_gr_ttm.SetPointError(i,  bin_size/2. , ttm_bias.GetRMSError()  )
        reso_gr_nn.SetPoint(i, initial_bin + (i+0.5)*bin_size , nn_bias.GetRMS()  )
        reso_gr_nn.SetPointError(i,  bin_size/2. , nn_bias.GetRMSError()  )
        cut_reso_gr_nn.SetPoint(i, initial_bin + (i+0.5)*bin_size , cut_nn_bias.GetRMS()  )
        cut_reso_gr_nn.SetPointError(i,  bin_size/2. , cut_nn_bias.GetRMSError()  )       
        reso_gr_smin.SetPoint(i, initial_bin + (i+0.5)*bin_size , smin_bias.GetRMS()  )
        reso_gr_smin.SetPointError(i,  bin_size/2. , smin_bias.GetRMSError()  )
        reso_gr_mmc.SetPoint(i, initial_bin + (i+0.5)*bin_size , mmc_bias.GetRMS()  )
        reso_gr_mmc.SetPointError(i,  bin_size/2. , mmc_bias.GetRMSError()  )

        ttm_bias.Reset("ICESM")
        nn_bias.Reset("ICESM")
        cut_nn_bias.Reset("ICESM")
        smin_bias.Reset("ICESM")
        mmc_bias.Reset("ICESM")

    #ploting the relative biases and resolutions
    plot_mass_relativebias(gr_nn,gr_smin,gr_ttm_help,gr_mmc,cut_gr_nn,channel)
    plot_mass_relativereso(reso_gr_nn,reso_gr_smin,reso_gr_ttm,reso_gr_mmc,cut_reso_gr_nn,channel)

    return 0

def plot_mass_relativebias(nn,smin,ttm,mmc,nn_cut,channel = 'HH'):

    names = ['m_{T}^{Total}', 'm_{#tau#tau}^{NN}','#sqrt{s}^{min}','m_{#tau#tau}^{MMC}', 'm_{#tau#tau}^{NN-Cut}']
    color = [633,881,418,863,802]
    container = [ttm,nn,smin,mmc]

    i = 0
    for entry in container:
        entry.SetLineColor(  color[i]   )
        entry.SetLineWidth(2)
        entry.SetMarkerColor( color[i] )
        entry.SetMarkerStyle( 20 )
        entry.SetMarkerSize(1.1)
        i=i+1

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(50.0, 0.0, 2000.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    i=0
    for entry in container:
        ax.plot(entry,"p")
        legend.AddEntry(entry, names[i],"L")
        i=i+1
    ax.plot(line7)
    
    legend.Draw()
    ax.set_xlim(50,2000)
    ax.set_ylim(-1.2,0.5)

    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel("true m_{#tau#tau}  [GeV]")
    ax.set_ylabel("Relative Bias")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax.text(0.2, 0.86, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    if( 'LH' in  str(channel) ):
        ax.text(0.2, 0.815, "#tau_{H}#tau_{L}, signal region", size=22, align=13)
    else:
        ax.text(0.2, 0.815, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("bias_"+str(channel)+".png")
    fig.savefig("bias_"+str(channel)+".pdf")

    return 0

def plot_mass_relativereso(nn,smin,ttm,mmc,nn_cut, channel = 'HH'):

    names = ['m_{T}^{Total}', 'm_{#tau#tau}^{NN}','#sqrt{s}^{min}','m_{#tau#tau}^{MMC}', 'm_{#tau#tau}^{NN - Cut}']
    color = [633,881,418,863,802]
    container = [ttm,nn,smin,mmc]

    i = 0
    for entry in container:
        entry.SetLineColor(  color[i]   )
        entry.SetLineWidth(2)
        entry.SetMarkerColor( color[i] )
        entry.SetMarkerStyle( 20 )
        entry.SetMarkerSize(1.1)
        i=i+1

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(50.0, 0.0, 2000.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    i=0
    for entry in container:
        ax.plot(entry,"p")
        legend.AddEntry(entry, names[i],"L")
        i=i+1
    ax.plot(line7)
    
    legend.Draw()
    ax.set_xlim(50,2000)
    ax.set_ylim(0,0.9)

    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel("true m_{#tau#tau}  [GeV]")
    ax.set_ylabel("Relative Resolution")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax.text(0.2, 0.86, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    if( 'LH' in  str(channel) ):
        ax.text(0.2, 0.815, "#tau_{H}#tau_{L}, signal region", size=22, align=13)
    else:
        ax.text(0.2, 0.815, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("resolution_" + str(channel)+".png")
    fig.savefig("resolution_"+  str(channel)+".pdf")

    return 0    


def plot_1d_distributions(distri,true_dist,names,mmc,ttm,smin,t_massa):

    # Set the ATLAS Style
    aplt.set_atlas_style()

    #ploting the distributions for all outputs xD
    for i in range(len(true_dist)):

        # Create a figure and ax1es
        fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)


        #true_dist[i] = true_dist[i].Rebin(len(posi)-1, names[i],posi)
        #distri[i] = distri[i].Rebin(len(posi)-1, names[i],posi)

        true_dist[i].Rebin(10)
        distri[i].Rebin(10)

        #tru_tru.Scale(  true_dist[0].Integral()/tru_tru.Integrate() )

        #tru_tru.Rebin(5)

        #adding legends:
        legend = root.TLegend(0.60, 0.62, 0.88, 0.92)
        legend.SetFillColorAlpha(1, 0)   #what is this for?
        legend.SetTextSize(26)
        legend.SetFillColorAlpha(0, 0)


        if( 'mass' in str(names[i]) ):
            mmc.Rebin(10)
            ttm.Rebin(10)
            smin.Rebin(10)
            t_massa.Rebin(10)
            #r_massa.Rebin(10)

            mmc.SetLineColor(863)
            ttm.SetLineColor(633)
            smin.SetLineColor(418)
            t_massa.SetLineColor(880)
            #r_massa.SetLineColor(880)
            #legend.AddEntry(t_massa, "truth - func","L")


            mmc.SetLineWidth(3)
            ttm.SetLineWidth(3)
            smin.SetLineWidth(3)
            t_massa.SetLineWidth(3)
            #r_massa.SetLineWidth(3)

            ax1.plot(mmc)
            ax1.plot(smin)
            ax1.plot(ttm)
            ax1.plot(t_massa)
            #ax1.plot(r_massa)

            legend.AddEntry(mmc, "m_{#tau#tau}^{MMC}","L")
            legend.AddEntry(ttm, "m_{T}^{total}","L")
            legend.AddEntry(smin, "#sqrt{s}^{min}","L")
            #legend.AddEntry(r_massa, "m_{#tau#tau}^{Reco}","L")
        else:
            pass

        true_dist[i].SetLineColor(1)
        distri[i].SetLineColor(881)

        legend.AddEntry(distri[i], "m_{#tau#tau}^{NN}","L")
        legend.AddEntry(true_dist[i], "m_{#tau#tau}^{True}","L")

        true_dist[i].SetLineWidth(3)
        distri[i].SetLineWidth(3)

        ax1.plot(true_dist[i])
        ax1.plot(distri[i])
        #ax1.plot(tru_tru)
        ax1.set_ylim(0.01, 1e+08 ) #x100 to give space for the text in the image

        ax1.set_yscale("log")

        # Draw line at y=1 in ratio panel
        line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
        ax2.plot(line)

        # Calculate and draw the ratio
        ratio_hist = true_dist[i].Clone()
        ratio_hist.Divide(distri[i])
        ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
        
        ratio_hist = true_dist[i].Clone()
        ratio_hist.Divide(ttm)

        ratio_graph_ttm = aplt.root_helpers.hist_to_graph(ratio_hist)
        ratio_graph_ttm.SetMarkerColor(633)

        ax2.plot(ratio_graph, "P0")
        ax2.plot(ratio_graph_ttm, "P0")
        ax2.set_ylim(0.65, 1.55)

        ax2.set_ylabel("Number of events")
        ax2.set_ylabel("truth/estimated", loc="centre")
        ax2.set_xlabel(names[i]+' [GeV]')

        # Add the ATLAS Label
        ax1.cd()
        aplt.atlas_label(text="Work in progress", loc="upper left")
        ax1.text(0.2, 0.852, "#sqrt{s} = 13 TeV, 138.9 fb^{-1}", size=22, align=13)
        ax1.text(0.2, 0.81, "#tau_{H}#tau_{H}, signal region", size=22, align=13)

        # Add legend
        legend.Draw()

        #saving the plots
        fig.savefig("distributions/dist_"+names[i]+ ".png")
        fig.savefig("distributions/dist_"+names[i]+ ".pdf")

    return 0
  
#now calculating the ditau pt bias and resolution xD  
def calculate_ptditau_bias_and_reso(targs,smin_ditaupt,r_pt,ttm_mass,thesemus,smin_mass,mmc_mass,pesos):
    bin_size = 25
    initial_bin = 0
    final_bin = 600

    n_iterations = int((final_bin - initial_bin)/bin_size)

    #creating the histograms and tgraph errors to plot the resolution and bias
    nn_bias = root.TH1F( 'nn_bias', 'nn_bias', 100, -5,5  )
    reco_bias = root.TH1F( 'reco_bias', 'reco_bias', 100, -5,5  )
    smin_bias = root.TH1F( 'smin_bias', 'smin_bias', 100, -5,5  )

    gr_nn = root.TGraphErrors()
    gr_smin = root.TGraphErrors()
    gr_reco = root.TGraphErrors()
    
    reso_gr_reco = root.TGraphErrors()
    reso_gr_nn = root.TGraphErrors()
    reso_gr_smin = root.TGraphErrors()

    #making sure the arays are flat numpy arrays
    keep_true_llpt = np.array(targs.detach().numpy()).reshape(len(targs))
    keep_reco_llpt = np.array(r_pt).reshape(len(r_pt))
    keep_nn_llpt = np.array(thesemus.detach().numpy()).reshape(len(thesemus))
    keep_smin_llpt = np.array(smin_mass).reshape(len(smin_mass))
    keep_pesos = np.array(pesos).reshape( len(pesos) ) 

    falso = True
    falso_2 = True
    falso_3 = True

    jj = 0
    for i in range(n_iterations):

        if( jj*bin_size >= 200 and falso ):
            bin_size = 50
            jj= 4
            falso= False

        if( jj*bin_size >= 300 and falso_3 ):
            bin_size = 100
            jj= 3
            falso_3= False

        if( jj*bin_size >= 400 and falso_2 ):
            bin_size = 200
            jj= 2
            falso_2= False 

        #reseting the arrays
        reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = keep_reco_llpt,keep_nn_llpt,keep_true_llpt,keep_smin_llpt,keep_pesos

        #applying the cut
        reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = reco_lpt[ true_lpt > initial_bin + (jj)*bin_size ],nn_lpt[ true_lpt > initial_bin + (jj)*bin_size ],true_lpt[ true_lpt > initial_bin + (jj)*bin_size ],smin_lpt[ true_lpt > initial_bin + (jj)*bin_size ],pesos[ true_lpt > initial_bin + (jj)*bin_size ]
        reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = reco_lpt[ true_lpt < initial_bin + (jj+1)*bin_size ],nn_lpt[ true_lpt < initial_bin + (jj+1)*bin_size ],true_lpt[ true_lpt < initial_bin + (jj+1)*bin_size ],smin_lpt[ true_lpt < initial_bin + (jj+1)*bin_size ],pesos[ true_lpt < initial_bin + (jj+1)*bin_size ]

        #rel_diff is a numpy array, i will fill a histogram with it and use root tools to extract mean and std and errors.
        nn_rel_diff = (nn_lpt - true_lpt)/(true_lpt)
        reco_rel_diff = (reco_lpt - true_lpt)/(true_lpt)
        smin_rel_diff = (smin_lpt-true_lpt)/(true_lpt)

        rnp.fill_hist(nn_bias,nn_rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(reco_bias,reco_rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(smin_bias,smin_rel_diff, weights=np.array(pesos).reshape(len(pesos)))

        #filling the Tgraph Errors graphs

        gr_reco.SetPoint(i, initial_bin + (jj+0.5)*bin_size , reco_bias.GetMean()  )
        gr_reco.SetPointError(i,  bin_size/2. , reco_bias.GetMeanError()  )

        gr_nn.SetPoint(i, initial_bin + (jj+0.5)*bin_size , nn_bias.GetMean()  )
        gr_nn.SetPointError(i,  bin_size/2. , nn_bias.GetMeanError()  )

        gr_smin.SetPoint(i, initial_bin + (jj+0.5)*bin_size , smin_bias.GetMean()  )
        gr_smin.SetPointError(i,  bin_size/2. , smin_bias.GetMeanError()  )        

        reso_gr_reco.SetPoint(i, initial_bin + (jj+0.5)*bin_size , reco_bias.GetRMS()  )
        reso_gr_reco.SetPointError(i,  bin_size/2. , reco_bias.GetRMSError()  )

        reso_gr_nn.SetPoint(i, initial_bin + (jj+0.5)*bin_size , nn_bias.GetRMS()  )
        reso_gr_nn.SetPointError(i,  bin_size/2. , nn_bias.GetRMSError()  )
     
        reso_gr_smin.SetPoint(i, initial_bin + (jj+0.5)*bin_size , smin_bias.GetRMS()  )
        reso_gr_smin.SetPointError(i,  bin_size/2. , smin_bias.GetRMSError()  )

        reco_bias.Reset("ICESM")
        nn_bias.Reset("ICESM")
        smin_bias.Reset("ICESM")

        if(jj*bin_size >= final_bin):
            break

        jj=jj+1

    plot_ptditau_relativebias(gr_nn,gr_smin,gr_reco)
    plot_ptditau_relativereso(reso_gr_nn,reso_gr_smin,reso_gr_reco)

    return 0 



def plot_ptditau_relativebias(nn,smin,reco):

    names = ['p_{T}^{#tau#tau}^{NN}','p_{T}^{#tau#tau}_{smin}','Reco p_{T}^{#tau#tau}' ]
    color = [881,418,802]
    container = [nn,smin,reco]

    i = 0
    for entry in container:
        entry.SetLineColor(  color[i]   )
        entry.SetLineWidth(3)
        entry.SetMarkerColor( color[i] )
        entry.SetMarkerStyle( 20 )
        entry.SetMarkerSize(1.1)
        i=i+1

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(0.0, 0.0, 600.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    i=0
    for entry in container:
        ax.plot(entry,"p")
        legend.AddEntry(entry, names[i],"L")
        i=i+1
    ax.plot(line7)
    
    legend.Draw()
    ax.set_xlim(0,600)
    ax.set_ylim(-0.8,0.6)

    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel("true p_{T}^{#tau#tau}  [GeV]")
    ax.set_ylabel("Relative Bias")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax.text(0.2, 0.86, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    ax.text(0.2, 0.815, "#tau_{H}#tau_{L}, signal region", size=22, align=13)


    fig.savefig("bias_pttautau.png")
    fig.savefig("bias_pttautau.pdf")

    return 0    

def plot_ptditau_relativereso(nn,smin,reco):

    names = ['p_{T}^{#tau#tau}^{NN}','p_{T}^{#tau#tau}_{smin}','Reco p_{T}^{#tau#tau}' ]
    color = [881,418,802]
    container = [nn,smin,reco]

    i = 0
    for entry in container:
        entry.SetLineColor(  color[i]   )
        entry.SetLineWidth(3)
        entry.SetMarkerColor( color[i] )
        entry.SetMarkerStyle( 20 )
        entry.SetMarkerSize(1.1)
        i=i+1

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(0.0, 0.0, 600.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    i=0
    for entry in container:
        ax.plot(entry,"p")
        legend.AddEntry(entry, names[i],"L")
        i=i+1
    ax.plot(line7)
    
    legend.Draw()
    ax.set_xlim(0,600)
    ax.set_ylim(0.0,1.0)

    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel("true p_{T}^{#tau#tau}  [GeV]")
    ax.set_ylabel("Relative Resolution")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax.text(0.2, 0.86, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    ax.text(0.2, 0.815, "#tau_{H}#tau_{L}, signal region", size=22, align=13)


    fig.savefig("resolution_pttautau.png")
    fig.savefig("resolution_pttautau.pdf")

    return 0        

def calculate_leading_tau_bias_and_reso(targs,r_lpt,thesemus,smin_mass,pesos,unc,channel='HH'):

    bin_size = 30
    initial_bin = 0
    final_bin = 600

    #making sure the arays are flat numpy arrays
    keep_true_lpt = np.array(targs.detach().numpy()).reshape(len(targs))
    keep_reco_lpt = np.array(r_lpt).reshape(len(r_lpt))
    keep_nn_lpt = np.array(thesemus.detach().numpy()).reshape(len(thesemus))
    keep_smin_lpt = np.array(smin_mass).reshape(len(smin_mass))
    keep_pesos = np.array(pesos).reshape( len(pesos) ) 

    print( 'infos aqui: ', np.shape(keep_reco_lpt), np.shape( keep_nn_lpt ), np.shape(keep_true_lpt), np.shape(keep_smin_lpt), np.shape(keep_pesos) )

    n_iterations = int((final_bin - initial_bin)/bin_size)

    #creating histograms and TGraph errors and stuff xD

    gr_nn = root.TGraphErrors()
    gr_reco = root.TGraphErrors()
    gr_smin = root.TGraphErrors()
    gr_help = root.TGraphErrors()

    reso_gr_reco = root.TGraphErrors()
    reso_gr_nn = root.TGraphErrors()
    reso_gr_smin = root.TGraphErrors()

    nn_bias = root.TH1F( 'nn_bias', 'nn_bias', 100, -5,5  )
    reco_bias = root.TH1F( 'reco_bias', 'reco_bias', 100, -5,5  )
    cut_nn_bias = root.TH1F( 'cut_nn_bias', 'cut_nn_bias', 100, -5,5  )
    smin_bias = root.TH1F( 'smin_bias', 'smin_bias', 100, -5,5  )

    for i in range(n_iterations):

        #reseting the arrays
        reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = keep_reco_lpt,keep_nn_lpt,keep_true_lpt,keep_smin_lpt,keep_pesos

        #applying the cut
        reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = reco_lpt[ true_lpt > initial_bin + (i)*bin_size ],nn_lpt[ true_lpt > initial_bin + (i)*bin_size ],true_lpt[ true_lpt > initial_bin + (i)*bin_size ],smin_lpt[ true_lpt > initial_bin + (i)*bin_size ],pesos[ true_lpt > initial_bin + (i)*bin_size ]
        reco_lpt,nn_lpt,true_lpt,smin_lpt,pesos = reco_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],nn_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],true_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],smin_lpt[ true_lpt < initial_bin + (i+1)*bin_size ],pesos[ true_lpt < initial_bin + (i+1)*bin_size ]

        #rel_diff is a numpy array, i will fill a histogram with it and use root tools to extract mean and std and errors.
        nn_rel_diff = (nn_lpt - true_lpt)/(true_lpt)
        reco_rel_diff = (reco_lpt - true_lpt)/(true_lpt)
        smin_rel_diff = (smin_lpt-true_lpt)/(true_lpt)

        rnp.fill_hist(nn_bias,nn_rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(reco_bias,reco_rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(smin_bias,smin_rel_diff, weights=np.array(pesos).reshape(len(pesos)))

        #filling the Tgraph Errors graphs

        gr_reco.SetPoint(i, initial_bin + (i+0.5)*bin_size , reco_bias.GetMean()  )
        gr_reco.SetPointError(i,  bin_size/2. , reco_bias.GetMeanError()  )

        gr_nn.SetPoint(i, initial_bin + (i+0.5)*bin_size , nn_bias.GetMean()  )
        gr_nn.SetPointError(i,  bin_size/2. , nn_bias.GetMeanError()  )

        gr_smin.SetPoint(i, initial_bin + (i+0.5)*bin_size , smin_bias.GetMean()  )
        gr_smin.SetPointError(i,  bin_size/2. , smin_bias.GetMeanError()  )        

        reso_gr_reco.SetPoint(i, initial_bin + (i+0.5)*bin_size , reco_bias.GetRMS()  )
        reso_gr_reco.SetPointError(i,  bin_size/2. , reco_bias.GetRMSError()  )

        reso_gr_nn.SetPoint(i, initial_bin + (i+0.5)*bin_size , nn_bias.GetRMS()  )
        reso_gr_nn.SetPointError(i,  bin_size/2. , nn_bias.GetRMSError()  )
     
        reso_gr_smin.SetPoint(i, initial_bin + (i+0.5)*bin_size , smin_bias.GetRMS()  )
        reso_gr_smin.SetPointError(i,  bin_size/2. , smin_bias.GetRMSError()  )

        reco_bias.Reset("ICESM")
        nn_bias.Reset("ICESM")
        cut_nn_bias.Reset("ICESM")
        smin_bias.Reset("ICESM")

    plot_leading_tau_relativebias(gr_nn,gr_smin,gr_reco,channel)
    plot_leading_tau_relativereso(reso_gr_nn,reso_gr_smin,reso_gr_reco,channel)


def plot_leading_tau_relativebias(nn,smin,reco,channel):

    names = ['MDN lead #tau p_{T}','smin leading #tau p_{T}','Reco leading tau p_{T}' ]
    color = [881,418,802]
    container = [nn,smin,reco]

    i = 0
    for entry in container:
        entry.SetLineColor(  color[i]   )
        entry.SetLineWidth(3)
        entry.SetMarkerColor( color[i] )
        entry.SetMarkerStyle( 20 )
        entry.SetMarkerSize(1.1)
        i=i+1

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(0.0, 0.0, 600.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.60, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    i=0
    for entry in container:
        ax.plot(entry,"p")
        legend.AddEntry(entry, names[i],"L")
        i=i+1
    ax.plot(line7)
    
    legend.Draw()
    ax.set_xlim(0,600)
    ax.set_ylim(-0.8,0.5)

    ax.add_margins(top=0.16)

    # Set axis titles
    if( 'LH' in  str(channel) ):
        ax.set_xlabel("true hadronic tau p_{T}  [GeV]")
    else:    
        ax.set_xlabel("true leading tau p_{T}  [GeV]")
    ax.set_ylabel("Relative Bias")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    if( 'LH' in  str(channel) ):
        ax.text(0.2, 0.815, "#tau_{H}#tau_{L}, signal region", size=22, align=13)
    else:
        ax.text(0.2, 0.815, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("bias_leadtau_"+str(channel)+".png")
    fig.savefig("bias_leadtau_"+str(channel)+".pdf")



    return 0   

def plot_leading_tau_relativereso(nn,smin,reco,channel):

    names = ['MDN lead #tau p_{T}','smin leading #tau p_{T}','Reco leading tau p_{T}' ]
    color = [881,418,802]
    container = [nn,smin,reco]

    i = 0
    for entry in container:
        entry.SetLineColor(  color[i]   )
        entry.SetLineWidth(3)
        entry.SetMarkerColor( color[i] )
        entry.SetMarkerStyle( 20 )
        entry.SetMarkerSize(1.1)
        i=i+1

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(0.0, 0.0, 600.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.60, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    i=0
    for entry in container:
        ax.plot(entry,"p")
        legend.AddEntry(entry, names[i],"L")
        i=i+1
    ax.plot(line7)
    
    legend.Draw()
    ax.set_xlim(0,600)
    ax.set_ylim(0,0.7)

    ax.add_margins(top=0.16)

    # Set axis titles
    if( 'LH' in  str(channel) ):
        ax.set_xlabel("true hadronic tau p_{T}  [GeV]")
    else:    
        ax.set_xlabel("true leading tau p_{T}  [GeV]")
    ax.set_ylabel("Relative Resolution")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax.text(0.2, 0.86, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    ax.text(0.2, 0.815, "#tau_{H}#tau_{L}, signal region", size=22, align=13)


    if( 'LH' in  str(channel) ):
        ax.text(0.2, 0.815, "#tau_{H}#tau_{L}, signal region", size=22, align=13)
    else:
        ax.text(0.2, 0.815, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("reso_leadtau_"+str(channel)+".png")
    fig.savefig("reso_leadtau_"+str(channel)+".pdf")



    return 0   



def plot_eff(eff,type):

    # Set the ATLAS Style
    aplt.set_atlas_style()

    #ploting the distributions for all outputs xD


    # Create a figure and ax1es
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)
    #true_dist[i] = true_dist[i].Rebin(len(posi)-1, names[i],posi)
    #distri[i] = distri[i].Rebin(len(posi)-1, names[i],posi)
    #tru_tru.Scale(  true_dist[0].Integral()/tru_tru.Integrate() )
    #tru_tru.Rebin(5)
    #adding legends:
    legend = root.TLegend(0.60, 0.62, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)
   
    eff.SetLineColor(863)

    eff.SetLineWidth(3)

    #r_massa.SetLineWidth(3)
    #ax1.plot(eff)

    #ax1.plot(r_massa)
    legend.AddEntry(eff, "rough eff","L")




    #ax1.plot(tru_tru)
     #x100 to give space for the text in the image
    #ax1.set_yscale("log")
    # Draw line at y=1 in ratio panel
    line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
    ax2.plot(line)
    # Calculate and draw the ratio

    line = root.TLine(0, 0.97, 2000, 0.97)
    ax1.plot(line)
    
    ratio_hist = eff.Clone()
    ratio_graph_ttm = aplt.root_helpers.hist_to_graph(ratio_hist)
    ratio_graph_ttm.SetMarkerColor(862)
    ax1.plot(ratio_graph_ttm, "P0")
    ax1.set_ylim(0.5,  2.0 )
    #ax2.plot(ratio_graph_ttm, "P0")
    ax2.set_ylim(0.65, 1.55)
    ax1.set_ylabel("m_{#tau#tau}^{NN - cut}/m_{#tau#tau}^{NN}")
    ax2.set_ylabel("truth/estimated", loc="centre")
    ax2.set_xlabel('m_{#tau#tau} [GeV]')
    # Add the ATLAS Label
    ax1.set_xlim(60,  2000 )
    ax2.set_xlim(60,  2000 )
    ax1.cd()
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax1.text(0.2, 0.852, "#sqrt{s} = 13 TeV, 138.9 fb^{-1}", size=22, align=13)
    ax1.text(0.2, 0.81, "#tau_{H}#tau_{H}, signal region", size=22, align=13)
    # Add legend
    legend.Draw()
    #saving the plots
    fig.savefig("eff_cut_"+type+".png")
    fig.savefig("eff_cut_"+type+".pdf")

    return 0

def calculate_z_eta( reco_tau1,reco_tau2,nn_tau1,nn_tau2,true_tau1,true_tau2,pesos ):
    
    #calculating the Z eta with the momentum output
    nn_zeta = utils.ditau_eta(nn_tau1,nn_tau2)
    reco_zeta = utils.ditau_eta(reco_tau1,reco_tau2)
    true_zeta = utils.ditau_eta( true_tau1,true_tau2 )

    #calculating the relative bias and resolution for the ditau eta
    init_bin = -5
    final_bin = 5
    steps = 20
    step_size = (final_bin-init_bin)/steps

    keep_nn = nn_zeta
    keep_reco = reco_zeta
    keep_true = true_zeta
    keep_pesos = pesos

    nn_bias = root.TH1F( 'nn_bias', 'nn_bias', 100, -5,5  )
    reco_bias = root.TH1F( 'reco_bias', 'reco_bias', 100, -5,5  )

    reso_gr_nn = root.TGraphErrors()
    gr_nn = root.TGraphErrors()

    reso_gr_reco = root.TGraphErrors()
    gr_reco = root.TGraphErrors()

    for i in range(steps):

        #reseting the arrays
        reco_zeta,nn_zeta,true_zeta,pesos = keep_reco,keep_nn,keep_true,keep_pesos

        #applying the cut
        reco_zeta,nn_zeta,true_zeta,pesos = reco_zeta[ true_zeta > init_bin + (i)*step_size ],nn_zeta[ true_zeta > init_bin + (i)*step_size ],true_zeta[ true_zeta > init_bin + (i)*step_size ],pesos[ true_zeta > init_bin + (i)*step_size ]
        reco_zeta,nn_zeta,true_zeta,pesos = reco_zeta[ true_zeta < init_bin + (i+1)*step_size ],nn_zeta[ true_zeta < init_bin + (i+1)*step_size ],true_zeta[ true_zeta < init_bin + (i+1)*step_size ],pesos[ true_zeta < init_bin + (i+1)*step_size ]

        #rel_diff is a numpy array, i will fill a histogram with it and use root tools to extract mean and std and errors.
        rel_diff = (nn_zeta - true_zeta)
        reco_rel_diff = (reco_zeta - true_zeta)

        rnp.fill_hist(nn_bias,rel_diff, weights=np.array(pesos).reshape(len(pesos)))
        rnp.fill_hist(reco_bias,reco_rel_diff, weights=np.array(pesos).reshape(len(pesos)))

        gr_nn.SetPoint(i, init_bin + (i+0.5)*step_size , nn_bias.GetMean()  )
        gr_nn.SetPointError(i,  step_size/2. , nn_bias.GetMeanError()  )   

        reso_gr_nn.SetPoint(i, init_bin + (i+0.5)*step_size , nn_bias.GetRMS()  )
        reso_gr_nn.SetPointError(i,  step_size/2. , nn_bias.GetRMSError()  )   


        gr_reco.SetPoint(i, init_bin + (i+0.5)*step_size , reco_bias.GetMean()  )
        gr_reco.SetPointError(i,  step_size/2. , reco_bias.GetMeanError()  )   

        reso_gr_reco.SetPoint(i, init_bin + (i+0.5)*step_size , reco_bias.GetRMS()  )
        reso_gr_reco.SetPointError(i,  step_size/2. , reco_bias.GetRMSError()  )  

        nn_bias.Reset("ICESM") 
        reco_bias.Reset("ICESM") 

    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(-5.0, 0.0, 5.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    gr_nn.SetLineColor(  881  )
    gr_nn.SetLineWidth(2)
    gr_nn.SetMarkerColor( 881 )
    gr_nn.SetMarkerStyle( 20 )
    gr_nn.SetMarkerSize(1.1)


    gr_reco.SetLineColor(  802  )
    gr_reco.SetLineWidth(2)
    gr_reco.SetMarkerColor( 802 )
    gr_reco.SetMarkerStyle( 20 )
    gr_reco.SetMarkerSize(1.1)

    ax.plot(gr_nn,"p")
    ax.plot(gr_reco,"p")
    ax.plot(line7)
    legend.AddEntry(gr_nn, "#eta^{nn}","L")
    legend.AddEntry(gr_reco, "#eta^{Reco}","L")
    i=i+1

    
    legend.Draw()
    ax.set_xlim(-5,5)
    ax.set_ylim(-1.4,2.0)

    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel("true #eta^{#tau#tau}")
    ax.set_ylabel("Absolute Bias")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax.text(0.2, 0.86, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    ax.text(0.2, 0.815, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("bias_eta.png")
    fig.savefig("bias_eta.pdf")

    # =============== ploting the relative resolution ===============================

    aplt.set_atlas_style()
    # Create a figure and axes
    fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))


    line7 = root.TLine(0.0, 0.0, 600.0, 0.0)
    line7.SetLineStyle(3)
    line7.SetLineWidth(2)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    reso_gr_nn.SetLineColor(  881  )
    reso_gr_nn.SetLineWidth(2)
    reso_gr_nn.SetMarkerColor( 881 )
    reso_gr_nn.SetMarkerStyle( 20 )
    reso_gr_nn.SetMarkerSize(1.1)

    reso_gr_reco.SetLineColor(  802  )
    reso_gr_reco.SetLineWidth(2)
    reso_gr_reco.SetMarkerColor( 802 )
    reso_gr_reco.SetMarkerStyle( 20 )
    reso_gr_reco.SetMarkerSize(1.1)


    ax.plot(reso_gr_nn,"p")
    ax.plot(reso_gr_reco,"p")
    legend.AddEntry(reso_gr_nn, "#eta^{nn}","L")
    legend.AddEntry(reso_gr_reco, "#eta^{Reco}","L")
    i=i+1

    
    legend.Draw()
    ax.set_xlim(-5,5)
    ax.set_ylim(0.3,1.5)

    ax.add_margins(top=0.16)

    # Set axis titles
    ax.set_xlabel("true #eta^{#tau#tau}")
    ax.set_ylabel("Absolute resolution")


    # Add the ATLAS Label
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax.text(0.2, 0.86, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    ax.text(0.2, 0.815, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("reso_ditaueta.png")
    fig.savefig("reso_ditaueta.pdf")    




    ##### end of rel bias and resolution plots

    #correcting the arrays 
    reco_zeta,nn_zeta,true_zeta,pesos = keep_reco,keep_nn,keep_true,keep_pesos

    #creating and filling root histograms
    nn = root.TH1F('nn_zeta','n_zeta', 40, -5,5)
    true = root.TH1F('true_zeta','true_zeta', 40, -5,5)
    reco = root.TH1F('reco_zeta','reco_zeta', 40, -5,5)

    rnp.fill_hist(nn,nn_zeta,weights=np.array(pesos).reshape(len(pesos)))
    rnp.fill_hist(true,true_zeta,weights=np.array(pesos).reshape(len(pesos)))
    rnp.fill_hist(reco,reco_zeta,weights=np.array(pesos).reshape(len(pesos)))

    #now ploting the zeta distibution:

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    nn.SetLineColor(881)
    reco.SetLineColor(802)
    true.SetLineColor(1)

    nn.SetLineWidth(3)
    reco.SetLineWidth(3)
    true.SetLineWidth(3)

    ax1.plot(nn)
    ax1.plot(reco)
    ax1.plot(true)
    
    legend.AddEntry(nn, "#eta^{NN}","L")
    legend.AddEntry(reco, "#eta^{Reco}","L")
    legend.AddEntry(true, "true #eta","L")

    ratio_hist = true.Clone()
    ratio_hist.Divide(nn)
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ratio_graph.SetMarkerColor(881)

    ratio_hist = true.Clone()
    ratio_hist.Divide(reco)
    ratio_graph_reco = aplt.root_helpers.hist_to_graph(ratio_hist)
    ratio_graph_reco.SetMarkerColor(802)

    ax2.plot(ratio_graph,"P")
    ax2.plot(ratio_graph_reco,"P")
    # Draw line at y=1 in ratio panel
    line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
    ax2.plot(line)

    ax2.set_ylabel("truth/estimated", loc="centre")

    ax1.set_ylim(0,2800)
    ax2.set_ylim(0.8,1.24)

    #ax.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel("ditau #eta")
    ax1.set_ylabel("Number of events/Bin")


    # Add the ATLAS Label
    ax1.cd()
    legend.Draw()
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax1.text(0.2, 0.85, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    ax1.text(0.2, 0.81, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("distributions/ditaueta.png")
    fig.savefig("distributions/ditaueta.pdf")



    return 0

def calculate_costhetastar( reco_tau1,reco_tau2,nn_tau1,nn_tau2,true_tau1,true_tau2,pesos ):
    
    reco_costhetastar=  utils.costhetastar(reco_tau1, reco_tau2)
    nn_costhetastar =  utils.costhetastar(nn_tau1, nn_tau2)
    true_costhetastar =  utils.costhetastar(true_tau1, true_tau2)

    #creating and filling root histograms
    nn = root.TH1F('nn_costheta','n_costheta', 30, -1,1)
    true = root.TH1F('true_costheta','true_costheta', 30, -1,1)
    reco = root.TH1F('reco_costheta','reco_costheta', 30, -1,1)

    rnp.fill_hist(nn,nn_costhetastar,weights=np.array(pesos).reshape(len(pesos)))
    rnp.fill_hist(true,true_costhetastar,weights=np.array(pesos).reshape(len(pesos)))
    rnp.fill_hist(reco,reco_costhetastar,weights=np.array(pesos).reshape(len(pesos)))

    file = root.TFile.Open( 'bora.root', "RECREATE" )
    nn.Write()
    file.Close()

    #begining of the plot
    aplt.set_atlas_style()
    # Create a figure and axes
    fig, (ax1, ax2) = aplt.ratio_plot(name="fig1", figsize=(800, 800), hspace=0.05)

    #adding legends:
    legend = root.TLegend(0.63, 0.71, 0.88, 0.92)
    legend.SetFillColorAlpha(1, 0)   #what is this for?
    legend.SetTextSize(26)
    legend.SetFillColorAlpha(0, 0)

    nn.SetLineColor(881)
    reco.SetLineColor(802)
    true.SetLineColor(1)

    nn.SetLineWidth(3)
    reco.SetLineWidth(3)
    true.SetLineWidth(3)

    ax1.plot(nn)
    ax1.plot(reco)
    ax1.plot(true)
    
    legend.AddEntry(nn, "cos#theta*_{NN}","L")
    legend.AddEntry(reco, "cos#theta*_{Reco}","L")
    legend.AddEntry(true, "true cos#theta*","L")

    ratio_hist = true.Clone()
    ratio_hist.Divide(nn)
    ratio_graph = aplt.root_helpers.hist_to_graph(ratio_hist)
    ratio_graph.SetMarkerColor(881)

    ratio_hist = true.Clone()
    ratio_hist.Divide(reco)
    ratio_graph_reco = aplt.root_helpers.hist_to_graph(ratio_hist)
    ratio_graph_reco.SetMarkerColor(802)

    ax2.plot(ratio_graph,"P")
    ax2.plot(ratio_graph_reco,"P")
    # Draw line at y=1 in ratio panel
    line = root.TLine(ax1.get_xlim()[0], 1, ax1.get_xlim()[1], 1)
    ax2.plot(line)

    ax2.set_ylabel("truth/estimated", loc="centre")

    ax1.set_ylim(0,3500)
    ax2.set_ylim(0.8,1.24)

    #ax.add_margins(top=0.16)

    # Set axis titles
    ax2.set_xlabel("cos#theta*")
    ax1.set_ylabel("Number of events/Bin")


    # Add the ATLAS Label
    ax1.cd()
    legend.Draw()
    aplt.atlas_label(text="Work in progress", loc="upper left")
    ax1.text(0.2, 0.85, "#sqrt{s}=13 TeV, 139 fb^{-1}", size=22, align=13)
    ax1.text(0.2, 0.81, "#tau_{H}#tau_{H}, signal region", size=22, align=13)


    fig.savefig("distributions/costhetastar.png")
    fig.savefig("distributions/costhetastar.pdf")



    return 0